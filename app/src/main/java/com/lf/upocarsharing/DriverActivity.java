package com.lf.upocarsharing;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.lf.upocarsharing.model.Trip;

import java.util.ArrayList;
import java.util.Calendar;

public class DriverActivity extends AppCompatActivity {

    String driverCode;
    ArrayList<String> passangerRemove;

    Trip t;

    final String TAG = "bbb";

    private FirebaseFirestore db;

    LinearLayout llNotExit, llExit;
    Button btnDone, btnRemove, btnUpdate, btnPassangerList;
    EditText edtxtPickUp, edtxtDestination, edtxtSeat;
    TextView txtDriver, txtTime;
    ProgressBar progressBar;
    Intent toPassanger;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver);

        toPassanger = new Intent(this, PassengerActivity.class);

        driverCode = getIntent().getStringExtra(Constant.DRIVER);
        passangerRemove = new ArrayList<>();

        llExit = findViewById(R.id.llExits);
        llNotExit = findViewById(R.id.llNotExit);

        btnDone = findViewById(R.id.btnDone);
        btnRemove = findViewById(R.id.btnRemove);
        btnUpdate = findViewById(R.id.btnUpdate);
        btnPassangerList = findViewById(R.id.btnPassangerList);

        txtTime = findViewById(R.id.txtTime);

        edtxtPickUp = findViewById(R.id.edtxtPickUp);
        edtxtDestination = findViewById(R.id.edtxtDestination);
        edtxtSeat = findViewById(R.id.edtxtSeat);

        txtDriver = findViewById(R.id.txtDriver);

        progressBar = findViewById(R.id.progressBar4);

        db = FirebaseFirestore.getInstance();

        DriverActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run()
            {
                txtDriver.setText(driverCode);
                checkExits();
           }
        });

        buttonManager();

    }

    private void buttonManager(){
        btnRemove.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(t != null){
                    progressBar.setVisibility(View.VISIBLE);
                    db.collection(Constant.FB_TRIP).document(t.codeTrip)
                            .delete()
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    Log.d(TAG, "DocumentSnapshot successfully deleted!");

                                    //remove passanger from database
                                    db.collection(Constant.FB_PASSANGER)
                                            .whereEqualTo("codeTrip", t.codeTrip)
                                            .get()
                                            .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                                @Override
                                                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                                    if (task.isSuccessful()) {
                                                        for (QueryDocumentSnapshot document : task.getResult()) {
                                                            //  Log.d(TAG, document.getId() + " => " + document.getData());
                                                            passangerRemove.add(document.getId());
                                                            db.collection(Constant.FB_PASSANGER).document(document.getId())
                                                                    .delete()
                                                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                                        @Override
                                                                        public void onSuccess(Void aVoid) {
                                                                            Log.d(TAG, "Passanger successfully deleted!");
                                                                        }
                                                                    })
                                                                    .addOnFailureListener(new OnFailureListener() {
                                                                        @Override
                                                                        public void onFailure(@NonNull Exception e) {
                                                                            Log.w(TAG, "Error deleting passanger", e);
                                                                        }
                                                                    });
                                                        }

                                                    } else {
                                                        Log.d(TAG, "Error getting Errore: ", task.getException());
                                                    }
                                                }
                                            });

                                    cleanAll();
                                    llExit.setVisibility(View.GONE);
                                    llNotExit.setVisibility(View.VISIBLE);
                                    progressBar.setVisibility(View.GONE);
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Log.w(TAG, "Error deleting document", e);
                                }
                            });
                }
            }
        });


        btnDone.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(checkField()){
                    progressBar.setVisibility(View.VISIBLE);
                    DocumentReference ref = db.collection(Constant.FB_TRIP).document();
                    String id = ref.getId();
                    long seat = Long.parseLong(edtxtSeat.getText().toString());
                    t = new Trip(
                            id,
                            driverCode,
                            txtTime.getText().toString(),
                            edtxtPickUp.getText().toString(),
                            edtxtDestination.getText().toString(),
                            seat,
                            seat-1
                    );
                    db.collection(Constant.FB_TRIP).document(id).set(t);
                    llExit.setVisibility(View.VISIBLE);
                    llNotExit.setVisibility(View.GONE);
                    progressBar.setVisibility(View.GONE);
                }
            }
        });


        btnUpdate.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                long seat = Long.parseLong(edtxtSeat.getText().toString());
                t.timeDepartaure = txtTime.getText().toString();
                t.from = edtxtPickUp.getText().toString();
                t.to = edtxtDestination.getText().toString();
                t.seat = seat;
                t.seatAvailable= seat-1;

                db.collection(Constant.FB_TRIP).document(t.codeTrip).set(t).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {

                        DriverActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run()
                            {
                                checkExits();
                            }
                        });

                    }
                })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.w(TAG, "Error writing document", e);
                            }
                        });
            }
        });

        btnPassangerList.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                toPassanger.putExtra(Constant.TRIP, t.codeTrip );
                toPassanger.putExtra(Constant.DRIVER, driverCode );
                toPassanger.putExtra(Constant.PASSANGER, driverCode );
                startActivityForResult(toPassanger, 1);
            }
        });


        txtTime.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(DriverActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        txtTime.setText( selectedHour + ":" + selectedMinute);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });
    }

    private void checkExits(){

        db.collection(Constant.FB_TRIP)
                .whereEqualTo("codeDriver", driverCode)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                t = document.toObject(Trip.class);
                                edtxtPickUp.setText(t.from);
                                edtxtDestination.setText(t.to);
                                txtTime.setText(t.timeDepartaure);
                                edtxtSeat.setText(Long.toString(t.seat));
                                Log.d(TAG, "Found Trip: " + t.codeTrip + " => " + t.codeDriver);

                                llExit.setVisibility(View.VISIBLE);
                                llNotExit.setVisibility(View.GONE);
                            }
                        } else {
                             Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });
        progressBar.setVisibility(View.GONE);

    }

    private boolean checkField(){

        if(txtTime.getText().toString() == "")
            return false;

        if(edtxtDestination.getText().toString() == "")
            return false;

        if(edtxtPickUp.getText().toString() == "")
            return false;

        if(edtxtSeat.getText().toString() == "")
            return false;

        return true;
    }

    private void cleanAll(){
        txtTime.setText(R.string.select_time);
        edtxtDestination.setText("");
        edtxtPickUp.setText("");
        edtxtSeat.setText("");
    }

}
