package com.lf.upocarsharing;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.lf.upocarsharing.model.Trip;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    Intent toTrip, toDriver, toLogin, toPassangerTrip;
    Button btnLogout, btnTrip, btnDriver, btnMyTrip, btnPassangerTrip;
    LinearLayout llMyTrip, llProposeTrip;
    ProgressBar progressBar;
    List<Trip> trips;

    String code;

    private FirebaseAuth auth;
    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide(); // hide the title bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN); //enable full screen

        setContentView(R.layout.activity_main);

        llMyTrip = findViewById(R.id.llMyTrip);
        llProposeTrip = findViewById(R.id.llProposeTrip);

        btnTrip = findViewById(R.id.btnTrip);
        btnMyTrip = findViewById(R.id.btnMyTrip);
        btnLogout = findViewById(R.id.btnLogout);
        btnDriver = findViewById(R.id.btnDriver);
        btnPassangerTrip = findViewById(R.id.btnPassangerTrip);
        progressBar = findViewById(R.id.progressBar);

        auth = FirebaseAuth.getInstance();

        toTrip = new Intent(this, TripActivity.class);
        toPassangerTrip = new Intent(this, TripActivity.class);
        toDriver = new Intent(this, DriverActivity.class);
        toLogin = new Intent(this, LoginActivity.class);


        db = FirebaseFirestore.getInstance();
        trips = new ArrayList<>();

        if (auth.getCurrentUser() == null) {
            startActivity(toLogin);
            finish();
        }
        else{
            code = auth.getCurrentUser().getEmail();
        }

        toTrip.putExtra(Constant.PASSANGER, code);
        toPassangerTrip.putExtra(Constant.PASSANGER, code);
        toDriver.putExtra(Constant.DRIVER, code);

        btnTrip.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(toTrip);
            }
        });

        btnPassangerTrip.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                toPassangerTrip.putExtra(Constant.PASSANGER_TRIP, code);
                startActivity(toPassangerTrip);
            }
        });

        btnMyTrip.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(toDriver);
            }
        });

        btnDriver.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(toDriver);
            }
        });

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                auth.signOut();
                // this listener will be called when there is change in firebase user session
                FirebaseAuth.AuthStateListener authListener = new FirebaseAuth.AuthStateListener() {
                    @Override
                    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                    }
                };
                finish();
                startActivity(toLogin);
            }
        });


        new Thread(new Runnable() {
            @Override
            public void run()
            {
                //loadData();
            }
        }).start();


    }

    @Override
    protected void onResume() {
        super.onResume();
        llMyTrip.setVisibility(View.GONE);
        llProposeTrip.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        trips.clear();
        new Thread(new Runnable() {
            @Override
            public void run()
            {
                loadData();
            }
        }).start();
    }

    private void loadData(){

        trips.clear();
        db.collection(Constant.FB_TRIP)
                .whereEqualTo("codeDriver", code)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Trip t = document.toObject(Trip.class);

                                if(t.seatAvailable < t.seat) {
                                    trips.add(t);
                                }
                            }

                            if(trips.size() > 0){
                                llMyTrip.setVisibility(View.VISIBLE);
                                llProposeTrip.setVisibility(View.GONE);
                            }

                            progressBar.setVisibility(View.GONE);

                        } else {
                        }
                    }
                });


    }


}
