package com.lf.upocarsharing.model;

public class Trip {

    public String codeTrip = "";
    public String codeDriver = "";
    public String timeDepartaure = "";
    public String from = "";
    public String to = "";
    public long seat = -1;
    public long seatAvailable = -1;
    public long rating;

    public Trip(){}

    public Trip(
            String codeTrip,
            String codeDriver,
            String timeDepartaure,
            String from,
            String to,
            long seat,
            long seatAvailable
    )
    {
        this.codeTrip = codeTrip;
        this.codeDriver = codeDriver;
        this.timeDepartaure = timeDepartaure;
        this.from = from;
        this.to = to;
        this.seat = seat;
        this.seatAvailable = seatAvailable;
    }

}
