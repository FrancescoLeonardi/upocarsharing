package com.lf.upocarsharing.model;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.lf.upocarsharing.Constant;
import com.lf.upocarsharing.R;

import java.util.List;

public class ListTripAdapter extends ArrayAdapter<Trip> {

    private int resourceLayout;
    private Context mContext;

    public ListTripAdapter(Context context, int resource, List<Trip> items) {
        super(context, resource, items);
        this.resourceLayout = resource;
        this.mContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(mContext);
            v = vi.inflate(resourceLayout, null);
        }

        Trip t = getItem(position);

        if (t != null) {

            TextView txtFrom = v.findViewById(R.id.txtFrom);
            TextView txtTo = v.findViewById(R.id.txtTo);
            TextView txtTime = v.findViewById(R.id.txtTime);
            TextView txtDriver = v.findViewById(R.id.txtDriver);
            TextView txtSeat = v.findViewById(R.id.txtSeat);
            RatingBar r = v.findViewById(R.id.ratingBar);


            txtFrom.setText(t.from);
            txtTo.setText(t.to);
            txtTime.setText(t.timeDepartaure);
            txtDriver.setText(Constant.getOnlyName(t.codeDriver));
            txtSeat.setText(Long.toString(t.seatAvailable));
            r.setRating(t.rating);
        }

        return v;
    }



}