package com.lf.upocarsharing.model;

public class RatingDriver {

    public String id;
    public String codeDriver = "";
    public String codePassanger = "";
    public long value = -1;

    public RatingDriver(){}

    public RatingDriver(
            String id,
            String codeDriver,
            String codePassanger,
            long value
    ){
        this.id = id;
        this.codeDriver = codeDriver;
        this.codePassanger = codePassanger;
        this.value = value;
    }
}
