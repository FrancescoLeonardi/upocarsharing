package com.lf.upocarsharing.model;

public class Passanger {

    public String codeUser = "";
    public String codeTrip = "";

    public Passanger(){}

    public Passanger(
            String codeUser,
            String codeTrip
    ){
        this.codeUser = codeUser;
        this.codeTrip = codeTrip;
    }
}
