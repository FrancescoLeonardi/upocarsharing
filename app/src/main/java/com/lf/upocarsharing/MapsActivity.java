package com.lf.upocarsharing;

import android.app.ProgressDialog;
import android.location.Address;
import android.location.Geocoder;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    LatLng from, to;
    private String tripFrom, tripTo;
    private double latFrom, lonFrom, latTo, lonTo;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        tripFrom = getIntent().getStringExtra(Constant.TRIP_FROM);
        tripTo = getIntent().getStringExtra(Constant.TRIP_TO);


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if(getCoordinateFromName(tripFrom) != null) {
            latFrom = getCoordinateFromName(tripFrom).getLatitude();
            lonFrom = getCoordinateFromName(tripFrom).getLongitude();
        }
        else{
            latFrom = 0;
            lonFrom = 0;
        }

        if(getCoordinateFromName(tripTo) != null) {
            latTo = getCoordinateFromName(tripTo).getLatitude();
            lonTo = getCoordinateFromName(tripTo).getLongitude();
        }else{
            latTo = 0;
            lonTo = 0;
        }


        from = new LatLng(latFrom, lonFrom);
        to = new LatLng(latTo, lonTo);

        // Add a marker in Sydney and move the camera
        /*
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));*/

        mMap.addMarker(new MarkerOptions().position(from).title("Departure"));

        mMap.addMarker(new MarkerOptions().position(to).title("Arrive"));


        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(from, 10));

    }

    private Address getCoordinateFromName(String loc) {
        try {
            Geocoder geocoderFrom = new Geocoder(MapsActivity.this);
            List<Address> address;
            address = geocoderFrom.getFromLocationName(loc, 1);

            if (address.size() > 0) {
                return address.get(0);
            } else
                Constant.showMessageOK(MapsActivity.this, "Impossible retrive departure location");


        } catch (Exception ex) {
            Constant.showMessageOK(MapsActivity.this, "Impossible retrive location");
        }
        return null;
    }

}
