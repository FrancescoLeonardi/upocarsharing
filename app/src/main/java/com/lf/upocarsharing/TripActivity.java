package com.lf.upocarsharing;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.lf.upocarsharing.model.ListTripAdapter;
import com.lf.upocarsharing.model.Trip;
import java.util.ArrayList;
import java.util.List;

public class TripActivity extends AppCompatActivity {

    String passangerCode;
    boolean passangerTrip;
    List<Trip> trips;
    ListView listView;
    Button btnSearch;
    EditText edtxtSearch;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        passangerTrip = false;
        passangerCode = getIntent().getStringExtra(Constant.PASSANGER);

        if(getIntent().getStringExtra(Constant.PASSANGER_TRIP)!=null)
            passangerTrip = true;

        edtxtSearch = findViewById(R.id.edtxtSearch);
        btnSearch = findViewById(R.id.btnSearch);
        listView = findViewById(R.id.listView);
        progressBar = findViewById(R.id.progressBar2);


        trips = new ArrayList<>();

        ListTripAdapter customAdapter = new ListTripAdapter(this, R.layout.trip_row, trips);
        listView.setAdapter(customAdapter);

        btnSearch.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TripActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run()
                    {
                        if(edtxtSearch.getText().toString().trim() == ""){
                            if(passangerTrip)
                                Constant.loadPassangerTrip(trips, listView, progressBar, passangerCode);
                            else
                                Constant.loadTrip(trips, listView, progressBar);
                        }
                        else {
                            if(passangerTrip)
                                Constant.loadPassangerTrip(trips, listView, progressBar, passangerCode, edtxtSearch.getText().toString());
                            else
                                Constant.loadTrip(trips, listView, progressBar, edtxtSearch.getText().toString());
                        }
                    }
                });
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();

        if(progressBar.getVisibility() != View.VISIBLE)
            progressBar.setVisibility(View.VISIBLE);

        final Intent toPassanger = new Intent(this, PassengerActivity.class);
        final Intent toMaps = new Intent(this, MapsActivity.class);

        TripActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run()
            {
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view,final int position, long id) {

                        TextView txtMaps = view.findViewById(R.id.txtMaps);
                        txtMaps.setOnClickListener(
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View arg0) {

                                        Trip t = trips.get(position);
                                        toMaps.putExtra(Constant.TRIP_FROM, t.from);
                                        toMaps.putExtra(Constant.TRIP_TO, t.to);

                                        startActivityForResult(toMaps, 1);

                                    }
                                });

                        toPassanger.putExtra(Constant.TRIP, trips.get(position).codeTrip );
                        toPassanger.putExtra(Constant.DRIVER, trips.get(position).codeDriver );
                        toPassanger.putExtra(Constant.PASSANGER, passangerCode);
                        startActivityForResult(toPassanger, 1);
                    }
                });

                if(passangerTrip)
                    Constant.loadPassangerTrip(trips, listView, progressBar, passangerCode);
                else
                    Constant.loadTrip(trips, listView, progressBar);
            }
        });
    }
}
