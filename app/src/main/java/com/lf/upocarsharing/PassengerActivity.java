package com.lf.upocarsharing;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.lf.upocarsharing.model.Passanger;
import com.lf.upocarsharing.model.RatingDriver;
import com.lf.upocarsharing.model.Trip;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PassengerActivity extends AppCompatActivity {

    Button btnDone, btnRemove;
    ListView lst;
    TextView txtDriver;
    ProgressBar progressBar;
    RatingBar ratingBar;

    String driverCode;
    String tripCode;
    String passangerCode;
    String passangerKey;

    boolean yet = false;

    public String TAG="aaa";

    Trip trip = null;
    RatingDriver ratingDriver = null;

    List<Passanger> passangers;
    private FirebaseFirestore db;

    Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_passanger);

        handler = new Handler();

        btnDone = findViewById(R.id.btnAddList);
        btnRemove = findViewById(R.id.btnRemove);
        lst = findViewById(R.id.listView);
        txtDriver = findViewById(R.id.txtDriver);
        progressBar = findViewById(R.id.progressBar3);
        ratingBar = findViewById(R.id.ratingBar);

        tripCode = getIntent().getStringExtra(Constant.TRIP);
        driverCode = getIntent().getStringExtra(Constant.DRIVER);
        passangerCode = getIntent().getStringExtra(Constant.PASSANGER);
        passangerKey = "";

        db = FirebaseFirestore.getInstance();

        btnDone.setVisibility(View.VISIBLE);
        btnRemove.setVisibility(View.GONE);

        passangers = new ArrayList<>();

        btnDone.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                addPassanger();
            }
        });
        btnRemove.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                removePassanger();
            }
        });

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener(){
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                if(ratingDriver == null)
                    getRating(ratingDriver.codeDriver, ratingDriver.codePassanger);

                ratingDriver.value = (long) ratingBar.getRating();
                updateRating();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        // get data from the table by the ListAdapter
        ListAdapter customAdapter = new ListAdapter(this, R.layout.passenger_row, passangers);
        lst.setAdapter(customAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(progressBar.getVisibility() != View.VISIBLE)
            progressBar.setVisibility(View.VISIBLE);
        PassengerActivity.this.runOnUiThread((new Runnable() {
            @Override
            public void run()
            {
                loadData();
            }
        }));
    }

    private void removePassanger(){
        if(trip != null){
            progressBar.setVisibility(View.VISIBLE);
                if (yet) {
                    db.collection(Constant.FB_PASSANGER)
                            .whereEqualTo("codeTrip", tripCode)
                            .get()
                            .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                    if (task.isSuccessful()) {
                                        for (QueryDocumentSnapshot document : task.getResult()) {

                                            db.collection(Constant.FB_PASSANGER).document(document.getId())
                                                    .delete()
                                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                        @Override
                                                        public void onSuccess(Void aVoid) {

                                                            yet = false;
                                                            btnDone.setVisibility(View.VISIBLE);
                                                            btnRemove.setVisibility(View.GONE);
                                                            trip.seatAvailable++;
                                                            updateTrip();
                                                            ratingDriver = null;
                                                            ratingBar.setIsIndicator(true);
                                                            deleteRatingDriver(driverCode, passangerCode);
                                                        }
                                                    })
                                                    .addOnFailureListener(new OnFailureListener() {
                                                        @Override
                                                        public void onFailure(@NonNull Exception e) {
                                                            Log.w(TAG, "Error deleting document", e);
                                                            progressBar.setVisibility(View.GONE);
                                                        }
                                                    });
                                        }
                                    }
                                }
                            });
                }
            }
    }

    private void addPassanger(){
        if(trip != null){
            progressBar.setVisibility(View.VISIBLE);
            if(!yet) {
                if (!passangerCode.contentEquals(trip.codeDriver)) {
                    if (trip.seatAvailable + 1 <= trip.seat) {
                        Passanger p = new Passanger(passangerCode, tripCode);
                        passangers.add(p);

                        db.collection(Constant.FB_PASSANGER).add(p)
                                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                    @Override
                                    public void onSuccess(DocumentReference documentReference) {

                                        yet = true;
                                        btnDone.setVisibility(View.GONE);
                                        btnRemove.setVisibility(View.VISIBLE);
                                        trip.seatAvailable--;
                                        updateTrip();
                                        ratingBar.setIsIndicator(false);
                                        newRating(driverCode, passangerCode, 0);
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Log.w(TAG, "Error adding document", e);
                                    }
                                });

                    }
                } else {
                    Constant.showMessageOK(PassengerActivity.this, "You are the driver");
                    ratingBar.setIsIndicator(true);
                    progressBar.setVisibility(View.GONE);
                }
            }
        }
    }

    private void updateTrip(){
        db.collection(Constant.FB_TRIP).document(trip.codeTrip).set(trip).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {

                passangers.clear();
                PassengerActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run()
                    {
                        loadData();
                    }
                });

            }
        })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error writing document", e);
                    }
                });
    }

    private void loadData(){

        progressBar.setVisibility(View.VISIBLE);
        ratingBar.setIsIndicator(true);
        DocumentReference docRef = db.collection(Constant.FB_TRIP).document(tripCode);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    Map<String, Object> map = document.getData();
                    if (document.exists()) {
                        trip = document.toObject(Trip.class);
                        Log.d(TAG, "Got it");

                        txtDriver.setText(Constant.getOnlyName(trip.codeDriver));

                        //Aggiunta clienti
                        passangers.clear();
                        db.collection(Constant.FB_PASSANGER)
                                .whereEqualTo("codeTrip", tripCode)
                                .get()
                                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                    @Override
                                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                        if (task.isSuccessful()) {
                                            for (QueryDocumentSnapshot document : task.getResult()) {
                                                //  Log.d(TAG, document.getId() + " => " + document.getData());
                                                Map<String, Object> map = document.getData();
                                                Passanger p = document.toObject(Passanger.class);
                                                passangers.add(p);
                                            }


                                            for(Passanger p : passangers){
                                                if(p.codeUser.contentEquals(passangerCode)) {
                                                    yet = true;
                                                    ratingBar.setIsIndicator(false);
                                                    getRating(driverCode, passangerCode);
                                                    if(ratingDriver == null)
                                                        ratingBar.setRating(0);
                                                    btnDone.setVisibility(View.GONE);
                                                    btnRemove.setVisibility(View.VISIBLE);
                                                    Log.d(TAG, "Yet in passanger");
                                                }
                                            }

                                            if(trip.seatAvailable == 0 && yet == false){
                                                btnDone.setVisibility(View.GONE);
                                                btnRemove.setVisibility(View.GONE);
                                            }

                                            for(long i = 0; i < trip.seatAvailable; i++) {
                                                passangers.add(new Passanger(
                                                        "Empty seat",
                                                        "Empty seat"
                                                ));
                                                Log.d(TAG, "Empty");
                                            }


                                            ((ArrayAdapter) lst.getAdapter()).notifyDataSetChanged();
                                            progressBar.setVisibility(View.GONE);
                                        } else {
                                            // Log.d(TAG, "Error getting documents: ", task.getException());
                                        }
                                    }
                                });


                    } else {
                         Log.d(TAG, "No such document");
                    }
                } else {
                      Log.d(TAG, "get failed with ", task.getException());
                }
            }
        });



    }

    public class ListAdapter extends ArrayAdapter<Passanger> {

        private int resourceLayout;
        private Context mContext;

        public ListAdapter(Context context, int resource, List<Passanger> items) {
            super(context, resource, items);
            this.resourceLayout = resource;
            this.mContext = context;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View v = convertView;

            if (v == null) {
                LayoutInflater vi;
                vi = LayoutInflater.from(mContext);
                v = vi.inflate(resourceLayout, null);
            }

            Passanger p = getItem(position);

            if (p != null) {
                TextView txtPassanger = v.findViewById(R.id.txtPassanger);
                txtPassanger.setText(p.codeUser);

            }

            return v;
        }

    }


    //region rating star

    public void getRating(final String codeDriver, String codePassanger) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection(Constant.FB_RATING_DRIVER)
                .whereEqualTo("codeDriver", codeDriver)
                .whereEqualTo("codePassanger", codePassanger)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                ratingDriver = document.toObject(RatingDriver.class);
                                ratingBar.setRating(ratingDriver.value);
                                break;
                            }

                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });

    }

    public  void deleteRatingDriver(String codeDriver, String codePassanger){
        final FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection(Constant.FB_RATING_DRIVER)
                .whereEqualTo("codeDriver", codeDriver)
                .whereEqualTo("codePassanger", codePassanger)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                ratingDriver = document.toObject(RatingDriver.class);
                                db.collection(Constant.FB_RATING_DRIVER).document(ratingDriver.id)
                                        .delete()
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                Log.w(TAG, "Delete Rating");
                                            }
                                        })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                Log.w(TAG, "Problem delete Rating");
                                            }
                                        });
                            }

                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });

    }

    public void newRating(String codeDriver, String codePassanger, long value){
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        DocumentReference ref = db.collection(Constant.FB_RATING_DRIVER).document();
        String id = ref.getId();

        ratingDriver = new RatingDriver(
                id,
                codeDriver,
                codePassanger,
                value
        );

        db.collection(Constant.FB_RATING_DRIVER).document(id).set(ratingDriver);
    }

    public void updateRating(){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        if(ratingDriver != null){
            db.collection(Constant.FB_RATING_DRIVER).document(ratingDriver.id).set(ratingDriver);
        }
    }



    //endregion
}
