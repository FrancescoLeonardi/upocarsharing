package com.lf.upocarsharing;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.lf.upocarsharing.model.Passanger;
import com.lf.upocarsharing.model.RatingDriver;
import com.lf.upocarsharing.model.Trip;import java.util.List;

public class Constant {
    final static String TRIP = "TRIP";
    final static String DRIVER = "DRIVER";
    final static String PASSANGER = "PASSANGER";
    final static String PASSANGER_TRIP = "PASSANGER_TRIP";

    final static String TAG = "Commons";

    final static String FB_TRIP = "trips";
    final static String FB_PASSANGER = "passanger";
    public final static String FB_RATING_DRIVER = "rating";

    final static String DOMAIN_EMAIL_1="studenti.uniupo.it";
    final static String DOMAIN_EMAIL_2="uniupo.it";

    final static String TRIP_FROM="FROM";
    final static String TRIP_TO="TO";

    public static void loadTrip(final List<Trip> trips, final ListView listView, final ProgressBar progressBar){

        trips.clear();
        final FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection(Constant.FB_TRIP)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                final Trip t = document.toObject(Trip.class);

                                if(t.seatAvailable < t.seat || t.seatAvailable == 0) {
                                    t.timeDepartaure = Constant.adjuctTimeView(t.timeDepartaure);
                                    trips.add(t);
                                }
                            }

                            for(int i = 0; i < trips.size(); i++){
                                final Trip t = trips.get(i);
                                final int tmp = i;
                                db.collection(Constant.FB_RATING_DRIVER)
                                        .whereEqualTo("codeDriver", t.codeDriver)
                                        .get()
                                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                            @Override
                                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                                if (task.isSuccessful()) {
                                                    long value = 0;
                                                    int number = 0;
                                                    for (QueryDocumentSnapshot document : task.getResult()) {
                                                        RatingDriver r = document.toObject(RatingDriver.class);
                                                        value += r.value;
                                                        number++;
                                                    }

                                                    if(number != 0)
                                                        t.rating = (value/number);
                                                    else
                                                        t.rating = 0;

                                                    if(tmp == trips.size() -1){
                                                        ((ArrayAdapter) listView.getAdapter()).notifyDataSetChanged();
                                                        progressBar.setVisibility(View.GONE);
                                                    }

                                                } else {
                                                    Log.d("SSS", "Error getting documents: ", task.getException());
                                                }
                                            }
                                        });

                            }


                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });
    }

    public static void loadPassangerTrip(final List<Trip> trips, final ListView listView, final ProgressBar progressBar, String passangerCode){

        trips.clear();
        final FirebaseFirestore db = FirebaseFirestore.getInstance();

        Log.d("SSSS", "loadPassangerTrip");

        db.collection(Constant.FB_PASSANGER)
                .whereEqualTo("codeUser", passangerCode )
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                final Passanger p = document.toObject(Passanger.class);

                                db.collection(Constant.FB_TRIP)
                                        .whereEqualTo("codeTrip", p.codeTrip)
                                        .get()
                                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                            @Override
                                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                                if (task.isSuccessful()) {
                                                    for (QueryDocumentSnapshot document : task.getResult()) {
                                                        final Trip t = document.toObject(Trip.class);
                                                        t.timeDepartaure = Constant.adjuctTimeView(t.timeDepartaure);
                                                        trips.add(t);
                                                    }

                                                    for(int i = 0; i < trips.size(); i++){
                                                        final Trip t = trips.get(i);
                                                        final int tmp = i;
                                                        db.collection(Constant.FB_RATING_DRIVER)
                                                                .whereEqualTo("codeDriver", t.codeDriver)
                                                                .get()
                                                                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                                                    @Override
                                                                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                                                        if (task.isSuccessful()) {
                                                                            long value = 0;
                                                                            int number = 0;
                                                                            for (QueryDocumentSnapshot document : task.getResult()) {
                                                                                RatingDriver r = document.toObject(RatingDriver.class);
                                                                                value += r.value;
                                                                                number++;
                                                                            }

                                                                            if(number != 0)
                                                                                t.rating = (value/number);
                                                                            else
                                                                                t.rating = 0;

                                                                            if(tmp == trips.size() -1){
                                                                                ((ArrayAdapter) listView.getAdapter()).notifyDataSetChanged();
                                                                                progressBar.setVisibility(View.GONE);
                                                                            }

                                                                        } else {
                                                                            Log.d("SSS", "Error getting documents: ", task.getException());
                                                                        }
                                                                    }
                                                                });

                                                    }


                                                } else {
                                                    Log.d(TAG, "Error getting documents: ", task.getException());
                                                }
                                            }
                                        });
                            }

                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });
    }

    public static void showMessageOK(Context con, String message) {
        new android.support.v7.app.AlertDialog.Builder(con)
                .setMessage(message)
                .setPositiveButton("OK", null)
                .create()
                .show();
    }

    public static void loadTrip(final List<Trip> trips, final ListView listView, final ProgressBar progressBar, final String conditionTrip){

        trips.clear();
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection(Constant.FB_TRIP)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Trip t = document.toObject(Trip.class);
                                t.timeDepartaure = Constant.adjuctTimeView(t.timeDepartaure);
                                if(t.seatAvailable < t.seat) {
                                    if(conditionTrip(t, conditionTrip))
                                        trips.add(t);
                                }
                            }

                            ((ArrayAdapter) listView.getAdapter()).notifyDataSetChanged();
                            progressBar.setVisibility(View.GONE);

                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });
    }

    public static void loadPassangerTrip(final List<Trip> trips, final ListView listView, final ProgressBar progressBar, String passangerCode, final String conditionTrip){

        trips.clear();
        final FirebaseFirestore db = FirebaseFirestore.getInstance();

        db.collection(Constant.FB_PASSANGER)
                .whereEqualTo("codeUser", passangerCode )
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                final Passanger p = document.toObject(Passanger.class);

                                db.collection(Constant.FB_TRIP)
                                        .whereEqualTo("codeTrip", p.codeTrip)
                                        .get()
                                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                            @Override
                                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                                if (task.isSuccessful()) {
                                                    for (QueryDocumentSnapshot document : task.getResult()) {
                                                        final Trip t = document.toObject(Trip.class);
                                                        t.timeDepartaure = Constant.adjuctTimeView(t.timeDepartaure);
                                                        if(conditionTrip(t, conditionTrip))
                                                            trips.add(t);
                                                    }

                                                    for(int i = 0; i < trips.size(); i++){
                                                        final Trip t = trips.get(i);
                                                        final int tmp = i;
                                                        db.collection(Constant.FB_RATING_DRIVER)
                                                                .whereEqualTo("codeDriver", t.codeDriver)
                                                                .get()
                                                                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                                                    @Override
                                                                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                                                        if (task.isSuccessful()) {
                                                                            long value = 0;
                                                                            int number = 0;
                                                                            for (QueryDocumentSnapshot document : task.getResult()) {
                                                                                RatingDriver r = document.toObject(RatingDriver.class);
                                                                                value += r.value;
                                                                                number++;
                                                                            }

                                                                            if(number != 0)
                                                                                t.rating = (value/number);
                                                                            else
                                                                                t.rating = 0;

                                                                            if(tmp == trips.size() -1){
                                                                                ((ArrayAdapter) listView.getAdapter()).notifyDataSetChanged();
                                                                                progressBar.setVisibility(View.GONE);
                                                                            }

                                                                        } else {
                                                                            Log.d("SSS", "Error getting documents: ", task.getException());
                                                                        }
                                                                    }
                                                                });

                                                    }


                                                } else {
                                                    Log.d(TAG, "Error getting documents: ", task.getException());
                                                }
                                            }
                                        });
                            }

                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });
    }

    public static boolean conditionTrip(Trip t, String condition){
        if(
                t.codeTrip.toLowerCase().contains(condition.toLowerCase()) ||
                t.codeDriver.toLowerCase().contains(condition.toLowerCase()) ||
                t.from.toLowerCase().contains(condition.toLowerCase()) ||
                t.to.toLowerCase().contains(condition.toLowerCase()) ||
                t.timeDepartaure.contains(condition)
        )
            return true;
        else
            return false;
    }

    public static String getOnlyName(String email){
        String[] splittedString = email.split("@");
        if(splittedString[0] != null){
            if(splittedString[0]!="")
                return splittedString[0];
        }
        return email;
    }

    public static boolean upoMail(String email){
        try {
            String[] domain = email.split("@");

            if(domain[0] == "")
                return false;
            if (domain[1].contentEquals(DOMAIN_EMAIL_1))
                return true;
            if (domain[1].contentEquals(DOMAIN_EMAIL_2))
                return true;

            return false;

        }catch (Exception ex){
            return false;
        }
    }

    public static String adjuctTimeView(String time){
        try{
            if(time.length() == 5)
                return time;
            String return_string;
            String[] split_time = time.split(":");

            if(split_time[0].length() < 2)
                return_string = "0"+split_time[0];
            else
                return_string = split_time[0];

            return_string += ":";

            if(split_time[1].length() < 2)
                return_string += "0"+split_time[1];
            else
                return_string += split_time[1];

            return return_string;

        }catch(Exception ex){
            return time;
        }
    }

}
